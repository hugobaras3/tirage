import csv
import random

"""
Un petit exemple utilisant `pdoc`.
"""

def read_csv(file_path):
    """
    Lit les données d'un fichier CSV.

    Paramètres :
        file_path (str) : Chemin vers le fichier CSV.

    Renvoie :
        list : Liste des lignes du fichier CSV.
    """
    with open(file_path, 'r') as file:
        reader = csv.reader(file, delimiter=';')
        next(reader, None)
        data = [row for row in reader]
    return data

def create_groups(csv_file):
    """
    Crée des paires de données d'étudiants à partir d'un fichier CSV.

    Paramètres :
        csv_file (str) : Chemin vers le fichier CSV contenant les données des étudiants.

    Renvoie :
        tuple : Un tuple contenant la liste de toutes les données des étudiants et une liste de paires (groupes).
    Lève :
        ValueError : Si le nombre d'étudiants n'est pas pair.
    """
    # Charge les données des étudiants depuis le CSV
    students_data = read_csv(csv_file)

    # Vérifie si le nombre d'étudiants est pair
    if len(students_data) % 2 == 0:
        # Mélange et crée des paires
        random.shuffle(students_data)
        groups = [students_data[i:i + 2] for i in range(0, len(students_data), 2)]
        return students_data, groups
    else:
        raise ValueError("Le nombre d'étudiants doit être pair pour former des groupes.")

def write_result_csv(output_file, groups):
    """
    Écrit les données groupées des étudiants dans un nouveau fichier CSV.

    Paramètres :
        output_file (str) : Chemin vers le fichier CSV de sortie.
        groups (list) : Liste de paires (groupes) contenant les données des étudiants.

    Lève :
        ValueError : Si un groupe n'a pas suffisamment d'étudiants pour former une paire.
    """
    with open(output_file, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(['Groupe N°', 'Etudiant 1', 'Nom', 'Prénom', 'Etudiant 2', 'Nom', 'Prénom'])

        for i, group in enumerate(groups, start=1):
            if len(group) == 2:
                student_1 = group[0]
                student_2 = group[1]
                writer.writerow([f'Groupe {i}', f'Etudiant 1:' ,student_1[1] ,student_1[0], f'Etudiant 2:', student_2[1], student_2[0]])
            else:
                raise ValueError(f"Le groupe {i} n'a pas suffisamment d'étudiants pour former une paire.")
