from functions import create_groups, write_result_csv
import argparse

parser = argparse.ArgumentParser(description="Programme de création de groupes aléatoires en fonction d'un fichier CSV")
parser.add_argument("-f", "--path", type=str, help="Chemin du fichier CSV")
args = parser.parse_args()

file = args.path

if file is not None:
    use_file = file
else:
    use_file = 'promotion_B3_B.csv'

if __name__ == '__main__':
    try:
        students, groups = create_groups(use_file)
        write_result_csv('resultats_groupes.csv', groups)
        print("Fichier résultat créé avec succès.")
    except ValueError as e:
        print(f"Erreur : {e}")
